package com.kanga.tp3.sms;

import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.kanga.tp3.R;

public class SmsActivity extends AppCompatActivity {

    private EditText Phone, Message;
    private ImageButton ENVOI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        initActivity();

    }
    /**
     * Initialisation
     */

    private void initActivity(){
        //RÉCUPÉRATION DES OBJETS GRAPHIQUES
        Phone = (EditText) findViewById(R.id.txtPhone);
        Message = (EditText) findViewById(R.id.txtMessage);
        ENVOI = (ImageButton) findViewById(R.id.btnEnvoi);

        //GESTION DE L'EVENEMENT CLIC SUR BOUTON ENVOI
        createOnClickEnvoiButton();
    }

    private void createOnClickEnvoiButton(){
        ENVOI.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                SmsManager.getDefault().sendTextMessage(Phone.getText().toString(), null, Message.getText().toString(), null,null );
            }
        });
    }

}
