package com.kanga.tp3.audio;

import android.annotation.SuppressLint;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kanga.tp3.R;

import java.util.concurrent.TimeUnit;

public class AudioActivity extends AppCompatActivity {

    TextView Position, Durée;
    SeekBar seekBar;
    ImageView btRew, btPlay, btPause, btff;


    MediaPlayer mediaPlayer;
    Handler handler = new Handler();
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        Position = findViewById(R.id.position);
        Durée = findViewById(R.id.durée);
        seekBar = findViewById(R.id.seek_bar);
        btRew = findViewById(R.id.bt_rew);
        btPlay = findViewById(R.id.bt_play);
        btPause = findViewById(R.id.bt_pause);
        btff = findViewById(R.id.bt_ff);

        mediaPlayer = MediaPlayer.create(this, R.raw.hillsong);

        runnable = new Runnable() {
            @Override
            public void run() {
                seekBar.setProgress(mediaPlayer.getCurrentPosition());

                handler.postDelayed(this,500);
            }
        };

        int durée = mediaPlayer.getDuration();

        String sdurée = convertFormat(durée);

        Durée.setText(sdurée);

        btPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btPlay.setVisibility(View.GONE);

                btPause.setVisibility(View.VISIBLE);

                mediaPlayer.start();

                seekBar.setMax(mediaPlayer.getDuration());

                handler.postDelayed(runnable,0);
            }
        });

        btPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btPause.setVisibility(View.GONE);

                btPlay.setVisibility(View.VISIBLE);

                mediaPlayer.pause();

                handler.removeCallbacks(runnable);
            }
        });

        btff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int positionActuelle = mediaPlayer.getCurrentPosition();
                int durée = mediaPlayer.getDuration();

                if (mediaPlayer.isPlaying() && durée != positionActuelle){
                    positionActuelle = positionActuelle + 5000;

                    Position.setText(convertFormat(positionActuelle));
                    mediaPlayer.seekTo(positionActuelle);

                }
            }
        });

        btRew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int positionActuelle = mediaPlayer.getCurrentPosition();
                if (mediaPlayer.isPlaying() && positionActuelle > 5000){

                    positionActuelle = positionActuelle - 5000;

                    Position.setText(convertFormat(positionActuelle));
                    mediaPlayer.seekTo(positionActuelle);
                }
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser){

                    mediaPlayer.seekTo(progress);
                }
                Position.setText(convertFormat(mediaPlayer.getCurrentPosition()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                btPause.setVisibility(View.GONE);

                btPlay.setVisibility(View.VISIBLE);

                mediaPlayer.seekTo(0);
            }
        });
    }
    @SuppressLint("DefaultLocale")
    private String convertFormat(int durée){
        return String.format("%02d:%02d"
        , TimeUnit.MILLISECONDS.toMinutes(durée)
        ,TimeUnit.MILLISECONDS.toSeconds(durée) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(durée)));
    }

}
