package com.kanga.tp3.geo;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kanga.tp3.R;

public class GeoActivity extends AppCompatActivity {

    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);

        webview = (WebView) findViewById(R.id.webGView);
        webview.setWebViewClient(new WebViewClient());
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webview.loadUrl("https://goo.gl/maps/eGcSPAtw5gWUAkHf8");
    }

    @Override
    public void onBackPressed() {
        if (webview.canGoBack()){
            webview.goBack();
            Toast.makeText( this,"Retour", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText( this,"Sortie", Toast.LENGTH_SHORT).show();
            super.onBackPressed();
        }

    }

}

