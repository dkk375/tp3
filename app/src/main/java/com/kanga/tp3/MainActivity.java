package com.kanga.tp3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.kanga.tp3.audio.AudioActivity;
import com.kanga.tp3.geo.GeoActivity;
import com.kanga.tp3.site.SiteActivity;
import com.kanga.tp3.sms.SmsActivity;
import com.kanga.tp3.tel.TelActivity;
import com.kanga.tp3.video.VideoActivity;

public class MainActivity extends Activity implements View.OnClickListener  {

    public Button BtnTel, BtnSms, BtnMusic, BtnVideo, BtnGéol, BtnSite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        BtnTel = (Button) findViewById(R.id.btnTel);
        BtnSms = (Button) findViewById(R.id.btnSms);
        BtnMusic = (Button) findViewById(R.id.btnAudio);
        BtnVideo = (Button) findViewById(R.id.btnVideo);
        BtnGéol = (Button) findViewById(R.id.BtnGeol);
        BtnSite = (Button) findViewById(R.id.BtnSite);


        BtnSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSmsActivity();
            }
        });

        BtnTel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTelActivity();
            }
        });

        BtnMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMusicActivity();
            }


        });

        BtnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openVideoActivity();
            }


        });

        BtnGéol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGeoActivity();
            }
        });

        BtnSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSiteActivity();
            }
        });



    }

    private void openGeoActivity() {
        Intent intent = new Intent(this, GeoActivity.class);
        startActivity(intent);
    }

    private void openSiteActivity() {
        Intent intent = new Intent(this, SiteActivity.class);
        startActivity(intent);
    }


    private void openMusicActivity() {
        Intent intent = new Intent(this, AudioActivity.class);
        startActivity(intent);
    }

    private void openTelActivity() {
        Intent intent = new Intent(this, TelActivity.class);
        startActivity(intent);
    }

    public void openSmsActivity(){
        Intent intent = new Intent(this, SmsActivity.class);
        startActivity(intent);
    }

    private void openVideoActivity() {
        Intent intent = new Intent(this, VideoActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {

    }

    }