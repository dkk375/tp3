package com.kanga.tp3.tel;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.kanga.tp3.R;

public class TelActivity extends AppCompatActivity {

    private EditText Phone;
    private ImageButton Appel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tel);

        Phone = findViewById(R.id.number);
        Appel = findViewById(R.id.appel);

        Appel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Contact = Phone.getText().toString();

                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:"+Contact));
                startActivity(intent);
            }
        });

    }
}
